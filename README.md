# Домашнее задание к занятию "3.8. Компьютерные сети, лекция 3"

> 1. Подключитесь к публичному маршрутизатору в интернет. Найдите маршрут к вашему публичному IP
```
telnet route-views.routeviews.org
Username: rviews
show ip route x.x.x.x/32
show bgp x.x.x.x/32
```
Листинг подключения к публичному маршрутизатору:

```
route-views>show ip route 85.249.43.231
Routing entry for 85.249.32.0/19
  Known via "bgp 6447", distance 20, metric 0
  Tag 3356, type external
  Last update from 4.68.4.46 1w0d ago
  Routing Descriptor Blocks:
  * 4.68.4.46, from 4.68.4.46, 1w0d ago
      Route metric is 0, traffic share count is 1
      AS Hops 2
      Route tag 3356
      MPLS label: none

route-views>show bgp 85.249.43.231
BGP routing table entry for 85.249.32.0/19, version 2301786153
Paths: (24 available, best #6, table default)
  Not advertised to any peer
  Refresh Epoch 1
  8283 6762 8402
    94.142.247.3 from 94.142.247.3 (94.142.247.3)
      Origin IGP, metric 0, localpref 100, valid, external
      Community: 6762:1 6762:92 6762:14900 8283:1 8283:101
      unknown transitive attribute: flag 0xE0 type 0x20 length 0x18
        value 0000 205B 0000 0000 0000 0001 0000 205B
              0000 0005 0000 0001
      path 7FE0C62F8F98 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  53767 174 174 3356 8402
    162.251.163.2 from 162.251.163.2 (162.251.162.3)
      Origin IGP, localpref 100, valid, external
      Community: 174:21000 174:22013 53767:5000
      path 7FE0A2523F18 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  3549 3356 8402
    208.51.134.254 from 208.51.134.254 (67.16.168.191)
      Origin IGP, metric 0, localpref 100, valid, external
      Community: 3356:2 3356:22 3356:100 3356:123 3356:501 3356:903 3356:2065 3549:2581 3549:30840 8402:900 8402:904
      path 7FE0C172BA30 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  19214 3257 3356 8402
    208.74.64.40 from 208.74.64.40 (208.74.64.40)
      Origin IGP, localpref 100, valid, external
      Community: 3257:8108 3257:30048 3257:50002 3257:51200 3257:51203
      path 7FE0491A12D8 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  701 1273 8402 8402
    137.39.3.55 from 137.39.3.55 (137.39.3.55)
      Origin IGP, localpref 100, valid, external
      path 7FE0FE394340 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  3356 8402
    4.68.4.46 from 4.68.4.46 (4.69.184.201)
      Origin IGP, metric 0, localpref 100, valid, external, best
      Community: 3356:2 3356:22 3356:100 3356:123 3356:501 3356:903 3356:2065 8402:900 8402:904
      path 7FE0E3D0E6B8 RPKI State not found
      rx pathid: 0, tx pathid: 0x0
  Refresh Epoch 1
  3561 3910 3356 8402
    206.24.210.80 from 206.24.210.80 (206.24.210.80)
      Origin IGP, localpref 100, valid, external
      path 7FE054D461A8 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  57866 3356 8402
    37.139.139.17 from 37.139.139.17 (37.139.139.17)
      Origin IGP, metric 0, localpref 100, valid, external
      Community: 3356:2 3356:22 3356:100 3356:123 3356:501 3356:903 3356:2065 8402:900 8402:904
      path 7FE111E143A8 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  7018 3356 8402
    12.0.1.63 from 12.0.1.63 (12.0.1.63)
      Origin IGP, localpref 100, valid, external
      Community: 7018:5000 7018:37232
      path 7FE1190FA008 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  4901 6079 3356 8402
    162.250.137.254 from 162.250.137.254 (162.250.137.254)
      Origin IGP, localpref 100, valid, external
      Community: 65000:10100 65000:10300 65000:10400
 path 7FE0118A4978 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  20912 3257 3356 8402
    212.66.96.126 from 212.66.96.126 (212.66.96.126)
      Origin IGP, localpref 100, valid, external
      Community: 3257:8070 3257:30515 3257:50001 3257:53900 3257:53902 20912:65004
      path 7FE0B39C8E60 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  3267 3216 8402
    194.85.40.15 from 194.85.40.15 (185.141.126.1)
      Origin IGP, metric 0, localpref 100, valid, external
      path 7FE18A41DFE8 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  3333 6762 8402
    193.0.0.56 from 193.0.0.56 (193.0.0.56)
      Origin IGP, localpref 100, valid, external
      Community: 6762:1 6762:92 6762:14900
      path 7FE0E562DE60 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  20130 6939 3356 8402
    140.192.8.16 from 140.192.8.16 (140.192.8.16)
      Origin IGP, localpref 100, valid, external
      path 7FE0D5A3ACC8 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  101 3356 8402
    209.124.176.223 from 209.124.176.223 (209.124.176.223)
      Origin IGP, localpref 100, valid, external
      Community: 101:20100 101:20110 101:22100 3356:2 3356:22 3356:100 3356:123 3356:501 3356:903 3356:2065 8402:900 8402:904
      Extended Community: RT:101:22100
      path 7FE13FCC7528 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  6939 3356 8402
    64.71.137.241 from 64.71.137.241 (216.218.252.164)
      Origin IGP, localpref 100, valid, external
      path 7FE09723BB48 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  1351 6939 3356 8402
    132.198.255.253 from 132.198.255.253 (132.198.255.253)
      Origin IGP, localpref 100, valid, external
      path 7FE17CB734A8 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  852 3356 8402
    154.11.12.212 from 154.11.12.212 (96.1.209.43)
      Origin IGP, metric 0, localpref 100, valid, external
      path 7FE1165A3C08 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  3303 6762 8402
    217.192.89.50 from 217.192.89.50 (138.187.128.158)
      Origin IGP, localpref 100, valid, external
      Community: 3303:1004 3303:1006 3303:3056 6762:1 6762:92 6762:14900
      path 7FE029783008 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 2
  2497 3356 8402
    202.232.0.2 from 202.232.0.2 (58.138.96.254)
      Origin IGP, localpref 100, valid, external
      path 7FE1253922E8 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
7660 2516 6762 8402
    203.181.248.168 from 203.181.248.168 (203.181.248.168)
      Origin IGP, localpref 100, valid, external
      Community: 2516:1030 7660:9003
      path 7FE0B14A9028 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  49788 12552 3216 8402
    91.218.184.60 from 91.218.184.60 (91.218.184.60)
      Origin IGP, localpref 100, valid, external
      Community: 12552:12000 12552:12700 12552:12701 12552:22000
      Extended Community: 0x43:100:1
      path 7FE11564C3F8 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  1221 4637 6762 8402
    203.62.252.83 from 203.62.252.83 (203.62.252.83)
      Origin IGP, localpref 100, valid, external
      path 7FE02EF331D0 RPKI State not found
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  3257 3356 8402
    89.149.178.10 from 89.149.178.10 (213.200.83.26)
      Origin IGP, metric 10, localpref 100, valid, external
      Community: 3257:8794 3257:30043 3257:50001 3257:54900 3257:54901
      path 7FE07E9E5340 RPKI State not found
      rx pathid: 0, tx pathid: 0
```

> 2. Создайте dummy0 интерфейс в Ubuntu. Добавьте несколько статических маршрутов. Проверьте таблицу маршрутизации.

Создаём интерфейс:
```
root@bav-vm:/home/user# sudo modprobe -v dummy

root@bav-vm:/home/user# lsmod | grep dummy
dummy                  16384  0

root@bav-vm:/home/user# ip link add dummy_test type dummy
root@bav-vm:/home/user# ip a | grep dummy
3: dummy_test: <BROADCAST,NOARP> mtu 1500 qdisc noop state DOWN group default qlen 1000

root@bav-vm:/home/user# ip addr add 172.16.77.150/24 dev dummy_test
root@bav-vm:/home/user# ip link set dummy_test up

root@bav-vm:/home/user# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:99:52:ac brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.31/24 metric 100 brd 192.168.122.255 scope global dynamic enp1s0
       valid_lft 2283sec preferred_lft 2283sec
    inet6 fe80::5054:ff:fe99:52ac/64 scope link
       valid_lft forever preferred_lft forever
3: dummy_test: <BROADCAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 26:3f:41:1c:ec:be brd ff:ff:ff:ff:ff:ff
    inet 172.16.77.150/24 scope global dummy_test
       valid_lft forever preferred_lft forever
    inet6 fe80::243f:41ff:fe1c:ecbe/64 scope link
       valid_lft forever preferred_lft forever

```

Добавляем статические маршруты:

```
root@bav-vm:/home/user# ip route add 192.168.60.0/24 via 192.168.122.1
root@bav-vm:/home/user# ip route add 192.168.70.0/24 dev dummy_test
root@bav-vm:/home/user# ip -br route
default via 192.168.122.1 dev enp1s0 proto dhcp src 192.168.122.31 metric 100
172.16.77.0/24 dev dummy_test proto kernel scope link src 172.16.77.150
192.168.60.0/24 via 192.168.122.1 dev enp1s0
192.168.70.0/24 dev dummy_test scope link
192.168.122.0/24 dev enp1s0 proto kernel scope link src 192.168.122.31 metric 100
192.168.122.1 dev enp1s0 proto dhcp scope link src 192.168.122.31 metric 100
```

> 3. Проверьте открытые TCP порты в Ubuntu, какие протоколы и приложения используют эти порты? Приведите несколько примеров.

```
root@bav-vm:/home/user# ss -tlpn
State       Recv-Q      Send-Q             Local Address:Port             Peer Address:Port      Process
LISTEN      0           4096               127.0.0.53%lo:53                    0.0.0.0:*          users:(("systemd-resolve",pid=626,fd=14))
LISTEN      0           128                      0.0.0.0:22                    0.0.0.0:*          users:(("sshd",pid=707,fd=3))
LISTEN      0           128                         [::]:22                       [::]:*          users:(("sshd",pid=707,fd=4))
```

53 порт слушает DNS клиент

22 порт слушает SSH

> 4. Проверьте используемые UDP сокеты в Ubuntu, какие протоколы и приложения используют эти порты?

```
root@bav-vm:/home/user# ss -ulpn
State      Recv-Q     Send-Q                   Local Address:Port           Peer Address:Port     Process
UNCONN     0          0                        127.0.0.53%lo:53                  0.0.0.0:*         users:(("systemd-resolve",pid=626,fd=13))
UNCONN     0          0                192.168.122.31%enp1s0:68                  0.0.0.0:*         users:(("systemd-network",pid=624,fd=15))
```

53 порт слушает DNS клиент

68 порт слушает Bootstrap Protocol Client

> 5. Используя diagrams.net, создайте L3 диаграмму вашей домашней сети или любой другой сети, с которой вы работали.

L3 Диаграмма домашней сети:

![L3 Диаграмма домашней сети](/Home_network.jpg)
